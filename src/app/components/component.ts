import { forEach } from "lodash";

export class Component  {
    public init(method: any) {
        document.addEventListener("DOMContentLoaded", method);
    }

    protected addEvent = (tag: any | any[], events: string[], callBack: any) => {
        let el: any = tag;
        if (typeof(tag) === "string") {
             el = document.querySelectorAll(tag);
        }
        Array.prototype.forEach.call(el, (element: HTMLElement) => {
            events.forEach((eventType: string, i) => {
                element.addEventListener(eventType, () => {
                    callBack(element, eventType, event);
                });
            });
        }, this);
    }

    protected updateElement = (element: NodeList | string, callBack: any) => {
        if (typeof(element) === "string") {
            element = document.querySelectorAll(element) as NodeList;
        }
        forEach(element, callBack);
    }

    protected addClass = (element: HTMLElement, className: string) => {
        if (element.classList) {
            element.classList.add(className);
        } else {
            element.className += " " + className;
        }
    }

    protected removeClass = (el: HTMLElement, className: string) => {
        if (el.classList) {
            el.classList.remove(className);
        } else {
        el.className = el.className.replace(new RegExp("(^|\\b)"
        + className.split(" ").join("|") + "(\\b|$)", "gi"), " ");
      }
    }
}
