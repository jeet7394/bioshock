import * as anime from "animejs";
import { Component } from "./../component";

export class Tabs extends Component {
    private width: number;
    private container: NodeList;
    private element: HTMLElement;
    private count: number;
    private currentView = 0;
    constructor() {
        super();
    }

    public init(element: HTMLElement) {
        this.element = element;
        this.container = element.querySelectorAll(".tabs-window");
        this.width = this.element.clientWidth;
        this.generateTabs(this.container.length);
        this.updateElement(this.container, this.addTranslateX);
    }

    private generateTabs(count: number) {
        const ul = this.element.querySelector(".tabs");
        const label = ul.querySelectorAll("a[goto]");
        this.addEvent(label, ["click"], this.labelClickHandler);
    }

    private labelClickHandler = (element: Element, eventType: string, event: Event) => {
        const goTo = parseInt(element.getAttribute("goto"), 10);
        if (this.currentView === goTo) {
            return;
        }
        this.animateTabs(goTo, this.currentView, goTo > this.currentView ? 1 : 0);
        this.currentView = goTo;
    }

    private animateTabs = (goTo: number, current: number, forward: number) => {
        const modifier = ["+=", "-="][forward];
        anime({
            easing: "easeOutQuad",
            targets: this.container,
            translateX: {
                duration: 400,
                value: modifier + (this.width * Math.abs(goTo - current)),
            },
        });
    }

    private addTranslateX = (element: HTMLElement) => {
        const container = Array.prototype.slice.call(this.container) as Element[];
        element.style.transform = `translateX(${container.indexOf(element) * this.width}px)`;
        element.style.display = "inline-block";
    }
}
