import * as anime from "animejs";
import { Component } from "./../component";

export class Model extends Component {
    constructor() {
        super();
        this.init(this.initialize);
    }
    public initialize = () => {
        this.addEvent(".modelopener", ["click"], this.goto);
    }
    public goto(element: any) {
        element = document.getElementById("div1");
        element.style.display = "block";
    }
}
