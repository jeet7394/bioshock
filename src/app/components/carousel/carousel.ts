import * as anime from "animejs";
import { Component } from "./../component";

export class Carousel extends Component {
    private width: number;
    private container: NodeList;
    private element: HTMLElement;
    private count: number;
    private currentView = 0;
    constructor() {
        super();
    }

    public init(element: HTMLElement) {
        this.element = element;
        this.container = element.querySelectorAll(".carousel-object-container");
        this.width = this.element.clientWidth;
        const tabs = element.querySelector(".tabs-container");
        if (tabs) {
            this.generateTabs(this.container.length, tabs);
        }
        this.updateElement(this.container, this.addTranslateX);
    }

    private generateTabs(count: number, tabs: Element) {
        const ul = document.createElement("ul");
        let li = "";
        for (let i = 0; i < count; i++) {
            li += `
                <li><div class="input-field">
                <input type="radio" name="courousel" value="">
                <label goto="${i}"></label>
                </div></li>
            `;
        }
        ul.innerHTML = li;
        tabs.appendChild(ul);
        const label = ul.querySelectorAll("label[goto]");
        this.addEvent(label, ["click"], this.labelClickHandler);
    }

    private labelClickHandler = (element: Element, eventType: string, event: Event) => {
        const goTo = parseInt(element.getAttribute("goto"), 10);
        if (this.currentView === goTo) {
            return;
        }
        this.animateCarousel(goTo, this.currentView, goTo > this.currentView ? 1 : 0);
        this.currentView = goTo;
    }

    private animateCarousel = (goTo: number, current: number, forward: number) => {
        const modifier = ["+=", "-="][forward];
        anime({
            easing: "easeOutQuad",
            targets: this.container,
            translateX: {
                duration: 400,
                value: modifier + (this.width * Math.abs(goTo - current)),
            },
        });
    }

    private addTranslateX = (element: HTMLElement) => {
        const container = Array.prototype.slice.call(this.container) as Element[];
        element.style.transform = `translateX(${container.indexOf(element) * this.width}px)`;
        element.style.display = "inline-block";
    }
}
