import * as anime from "animejs";
import { throttle } from "lodash";
import { Component } from "./../component";

export class FullPageScroll extends Component {
    private current = 1;
    private length: number;
    private scrolling = false;
    constructor() {
        super();
        this.init(this.initialize);
    }

    public initialize = () => {
        this.addEvent(".full-scroll", ["mousewheel", "DOMMouseScroll"], this.scrollHandler);
        this.addEvent([document], ["keydown"], this.handleKeyDown);
        this.length = document.querySelectorAll(`div[class^="full-page-"]`).length;
    }

    private scrollHandler = (element: HTMLElement, eventType: string, event: MouseWheelEvent) => {
        event.preventDefault();
        if (event.wheelDelta > 0 || event.detail < 0) {
            this.scrollUp(element);
        } else {
            this.scrollDown(element);
        }

    }

    private handleKeyDown = (element: HTMLElement, eventType: string, event: KeyboardEvent) => {
        element = element.querySelector(".full-scroll");
        if (event.which === 38) {
            this.scrollUp(element);
        } else if (event.which === 40) {
            this.scrollDown(element);
        }
    }

    private scrollUp = (element: HTMLElement) => {
        if (this.scrolling || this.current === 1) {
            return;
        }
        this.scrolling = true;
        const currentElement = element.querySelector(".full-page-" + this.current) as HTMLElement;
        const next = element.querySelector(".full-page-" + (this.current - 1)) as HTMLElement;
        this.removeClass(next, "in-active");
        this.addClass(next, "active");
        this.removeClass(currentElement, "active");
        this.addClass(currentElement, "in-active");
        this.addEvent(
            [next],
            ["animationend", "webkitAnimationEnd", "mozAnimationEnd", "transitionend"],
            this.UpdateScrolling,
        );
        this.current--;
    }

    private scrollDown = (element: HTMLElement) => {
        if (this.scrolling || this.length === this.current) {
            return;
        }
        this.scrolling = true;
        const currentElement = element.querySelector(".full-page-" + this.current) as HTMLElement;
        const next = element.querySelector(".full-page-" + (this.current + 1)) as HTMLElement;
        this.removeClass(next, "in-active");
        this.addClass(next, "active");
        this.removeClass(currentElement, "active");
        this.addClass(currentElement, "in-active");
        this.addEvent(
            [next],
            ["transitionend", "webkitTransitionend", "mozTransitionend", "animationend"],
            this.UpdateScrolling,
        );
        this.current++;
    }
    private UpdateScrolling = () => {
        setTimeout(() => {
            this.scrolling = false;
        }, 500);
    }
}
