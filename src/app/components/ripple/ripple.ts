import * as anime from "animejs";
import { Component } from "../component";

export class Ripple extends Component {
    private rippleDiv = `<div class="c-ripple js-ripple">
        <span class="c-ripple__circle"></span>
    </div>`;

    constructor() {
        super();
        this.init(this.initialize);
    }

    public initialize = () => {
        const rippleElement = document.querySelectorAll(".ripple") as NodeList;
        this.updateElement(rippleElement, this.addRippleDiv);
        const jsRipple = document.querySelectorAll(".js-ripple") as NodeList;
        this.addEvent(".js-ripple", ["click"], this.clickHandler);
    }

    private addRippleDiv = (element: HTMLElement) => {
        element.innerHTML += this.rippleDiv;
    }

    private clickHandler = (element: any, eventType: string, event: MouseEvent) => {
        const circle = element.querySelector(".c-ripple__circle") as HTMLElement;
        const x = event.pageX - element.parentNode.offsetLeft;
        const y = event.pageY - element.parentNode.offsetTop;
        circle.style.top = y + "px";
        circle.style.left = x + "px";
        this.animate(circle, event);
    }

    private animate = (element: any, event: MouseEvent) => {
        const animationTimeline = anime.timeline();
        animationTimeline.add({
            easing: "linear",
            opacity: {
                duration: 400,
                value: 1,
            },
            paddingBottom: {
                duration: 400,
                value: "200%",
            },
            targets: element,
            width: {
                duration: 400,
                value: "200%",
            },
        }).add({
            opacity: {
                delay: 0,
                duration: 100,
                value: 0,
            },
            paddingBottom: {
                duration: 100,
                value: "0%",
            },
            targets: element,
            width: {
                duration: 100,
                value: "0%",
            },
        });
    }
}
