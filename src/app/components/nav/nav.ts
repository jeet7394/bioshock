import { Component } from "./../component";

export class Nav extends Component {
    constructor() {
        super();
        this.init(this.initialize);
    }

    public initialize = () => {
        const navUl = document.querySelectorAll(".nav ul");
        this.updateElement(navUl, this.createIndicator);
        this.addEvent(".nav ul li", ["mouseover", "mouseout"], this.animateNavIndicator);
    }

    protected createIndicator = (ul: HTMLElement) => {
        const li = document.createElement("li");
        this.addClass(li, "nav-indicator");
        ul.appendChild(li);
    }

    protected animateNavIndicator = (el: HTMLElement, eventType: string) => {
        const width = el.offsetWidth;
        const left = el.offsetLeft;
        const parent = el.parentNode as HTMLElement;
        const indicator = parent.querySelector(".nav-indicator") as HTMLElement;
        if (eventType === "mouseover") {
            indicator.style.opacity = "1";
            indicator.style.width = width + "px";
            indicator.style.left = left + "px";
        } else {
            indicator.style.opacity = "0";
            indicator.style.left = 0 + "px";
        }
    }
}
