import * as anime from "animejs";
import { Component } from "./../component";

export class List extends Component {
    private listclicked = {
        opacity: "0",
        top: "0",
    };
    constructor() {
        super();
        this.init(this.initialize);
    }

    public initialize = () => {
        const navUl = document.querySelectorAll(".list ul");
        this.updateElement(navUl, this.createIndicator);
        this.addEvent(".list ul li", ["mouseover", "mouseout", "click"], this.animateListIndicator);
        this.addEvent(".feature-content", ["scroll"], this.onFeatureScroll);
    }

    public easeInOutExpo = (t: number, b: number, c: number, d: number) => {
        return (t === d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    }

    protected createIndicator = (ul: HTMLElement) => {
        const li = document.createElement("li");
        this.addClass(li, "list-indicator");
        ul.appendChild(li);
    }

    protected animateListIndicator = (el: HTMLElement, eventType: string) => {
        const height = el.offsetHeight;
        const top = el.offsetTop;
        const parent = el.parentNode as HTMLElement;
        const indicator = parent.querySelector(".list-indicator") as HTMLElement;
        if (eventType === "mouseover") {
            indicator.style.opacity = "1";
            indicator.style.height = height + "px";
            indicator.style.top = top + "px";
        } else if (eventType === "mouseout") {
            indicator.style.opacity = this.listclicked.opacity + "";
            indicator.style.top = this.listclicked.top + "px";
        } else {
            event.preventDefault();
            indicator.style.opacity = "1";
            indicator.style.top = top + "";
            this.listclicked = {
                opacity: "1",
                top: top + "",
            };
            this.checkIfFeature(el);
        }
    }

    protected checkIfFeature = (el: HTMLElement) => {
        const featureEl = el.offsetParent;
        const id = el.getAttribute("to");
        if (id) {
            const featureContentEl = document.getElementById(id);
            if (featureContentEl) {
                const parent = featureContentEl.parentNode as HTMLElement;
                this.scrollTo(parent, featureContentEl.offsetTop - featureContentEl.offsetHeight / 2, 1000);
            }
        }
    }

    protected scrollTo = (element: HTMLElement, to: number, duration: number) => {
        anime({
            easing: "easeOutQuad",
            scrollTop: {
                duration: 200,
                value: to,
            },
            targets: element,
        });
    }

    protected onFeatureScroll = (element: HTMLElement) => {
        if (element) {
            const parent = element.parentNode;
            const children = element.children;
            this.updateElement(children, this.animateFeature);
        }
    }

    protected animateFeature = (element: HTMLElement) => {
        const parent = element.parentNode.parentNode as HTMLElement;
        const indicator = parent.querySelector(".list-indicator") as HTMLElement;
        const childTop = parseInt(Math.abs(element.offsetTop
            - element.offsetHeight / 2).toFixed(0), 10);
        const childBot = parseInt(Math.abs(element.offsetTop
            + element.offsetHeight / 2).toFixed(0), 10);
        if (element.parentElement.scrollTop > childTop - 100 && element.parentElement.scrollTop < childBot - 100) {
            const id = element.id;
            if (id) {
                const li = document.querySelector("[to=" + id) as HTMLElement;
                if (li) {
                    indicator.style.opacity = "1";
                    indicator.style.height = li.offsetHeight + "px";
                    indicator.style.top = li.offsetTop + "px";
                }
            }
        }
    }
}
