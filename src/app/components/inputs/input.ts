import { Component } from "../component";

export class BSInput extends Component {
    private INPUT_WITH_LABEL_ANIMATIONS = [
        "text", "email", "number", "tel", "password",
        "search", "url", "textarea",
    ];

    constructor() {
        super();
        this.init(this.initialize);
    }

    public initialize = () => {
        this.addEvent("input", ["blur", "focus"], this.animateLabels);
        this.addEvent("textarea", ["blur", "focus"], this.animateLabels);
        this.addEvent("[type='radio'] ~ label", ["click"], this.updateRadio);
        this.addEvent("[type='checkbox'] ~ label", ["click"], this.updateCheckbox);
        this.addEvent("label", ["click"], this.inputLabelClickHandler);
        this.updateElement("input", this.handleValue);
    }

    private animateLabels = (element: HTMLInputElement, eventType: string) => {
        const parent = element.parentNode as HTMLElement;
        if (parent.classList.contains("input-field")) {
            const label = parent.querySelector("label");
            const prefix = parent.querySelector(".prefix") as HTMLElement;
            if (this.INPUT_WITH_LABEL_ANIMATIONS.indexOf(element.type) > -1) {
                this.addClassToLabel(label, "active", eventType, element);
                this.addClassToLabel(prefix, "active", eventType, element);
            }
            this.showHints(element, eventType);
        }
    }

    private showHints = (input: HTMLElement, eventType: string) => {
        const displayHandler: { [key: string]: string } = {
            blur: "none",
            focus: "block",
        };
        const changeDisplay = displayHandler[eventType];
        const parent = input.parentNode as HTMLElement;
        const hint = parent.querySelector(".hints") as HTMLElement;
        if (hint) {
            hint.style.display = changeDisplay;
        }
    }

    private inputLabelClickHandler = (label: HTMLElement, eventType: string) => {
        const parent = label.parentNode as HTMLElement;
        const inputField = parent.querySelector(
            "input, textarea",
        ) as HTMLInputElement;
        if (this.INPUT_WITH_LABEL_ANIMATIONS.indexOf(inputField.type) > -1) {
            inputField.focus();
        }
    }

    private addClassToLabel = (element: HTMLElement, newClass: string,
                               eventType: string,
                               inputElement: HTMLInputElement) => {
        if (element && element.classList && eventType === "focus") {
            element.classList.add(newClass);
        } else if (element && !element.classList && eventType === "focus") {
            element.className += " " + newClass;
        } else if (element && inputElement.value === "") {
            element.classList.remove(newClass);
        }
    }

    private updateRadio = (label: HTMLElement, event: string) => {
        const parent = label.parentNode as HTMLElement;
        if (parent.classList.contains("input-field")) {
            const radio = parent.querySelector("[type='radio']") as HTMLInputElement;
            radio.checked = true;
        }
    }

    private updateCheckbox = (label: HTMLElement, event: string) => {
        const parent = label.parentNode as HTMLElement;
        if (parent.classList.contains("input-field")) {
            const checkbox = parent.querySelector("[type='checkbox']") as HTMLInputElement;
            checkbox.checked = !checkbox.checked;
        }
    }

    private handleValue = (element: HTMLInputElement) => {
        if (element.value !== "" && element.value != null) {
            this.animateLabels(element, "focus");
        }
    }
}
