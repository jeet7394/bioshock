import { Carousel } from "./app/components/carousel/carousel";
import { FullPageScroll } from "./app/components/full-page-scroll/full-page-scroll";
import { BSInput } from "./app/components/inputs/input";
import { List } from "./app/components/list/list";
import { Model } from "./app/components/models/models";
import { Nav } from "./app/components/nav/nav";
import { Ripple } from "./app/components/ripple/ripple";
import { SkewScroll } from "./app/components/skew-scroll/skew-scroll";
import { Tabs } from "./app/components/tabs/tabs";
import "./app/main.scss";

const input = new BSInput();
const models = new Model();
const ripple = new Ripple();
const carousel = new Carousel();
const nav = new Nav();
const list = new List();
const skewScroll = new SkewScroll();
const fullPageScroll = new FullPageScroll();
const tab = new Tabs();

(document as any).Carousel = carousel;
(document as any).Tabs = tab;
