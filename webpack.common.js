 const path = require('path');
 const CleanWebpackPlugin = require('clean-webpack-plugin');
 const HtmlWebpackPlugin = require('html-webpack-plugin');
 const ExtractTextPlugin = require("extract-text-webpack-plugin");

 const extractSass = new ExtractTextPlugin({
   filename: "[name].bundle.css",
 });

 module.exports = {
   entry: {
     app: './src/index.ts'
   },
   module: {
     rules: [{
         test: /\.tsx?$/,
         use: 'ts-loader',
         exclude: /node_modules/
       },
       {
         test: /\.scss$/,
         use: extractSass.extract({
           use: [{
             loader: "css-loader"
           }, {
             loader: "sass-loader",
             options: {
               sourceMap: true
             }
           }]
         })
       }
     ]
   },
   resolve: {
     extensions: ['.tsx', '.ts', '.js', '.scss']
   },
   plugins: [
     new CleanWebpackPlugin(['dist']),
     extractSass
   ],
   output: {
     filename: '[name].bundle.js',
     path: path.resolve(__dirname, 'dist')
   },
   devtool: "source-map"
 };